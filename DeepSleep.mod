﻿<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="DeepSleep" version="1.0" date="03/03/2011" >

		<VersionSettings gameVersion="1.4.1" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Author name="Crestor" email="crestor@web.de" />
		<Description text="" />

		<Dependencies>
			<Dependency name="EATemplate_UnitFrames" />
			<Dependency name="EA_GroupWindow" />
			<Dependency name="EA_BattlegroupHUD" />
			<Dependency name="EA_OverheadMapWindow" />
		</Dependencies>

		<SavedVariables>
			<SavedVariable name="DeepSleep.data" />
		</SavedVariables>

		<Files>
			<File name="DeepSleep.lua" />
			<File name="Settings.xml" />
			<File name="Settings.lua" />
			<File name="Modules.lua" />
		</Files>

		<OnInitialize>
				<CallFunction name="DeepSleep.Initialize" />
				<CallFunction name="DeepSleep.Settings.Create" />
		</OnInitialize>

		<OnShutdown>
			<CallFunction name="DeepSleep.Shutdown" />
		</OnShutdown>

		<WARInfo>
			<Categories>
				<Category name="SYSTEM" />
				<Category name="DEVELOPMENT" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name= "MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
		</WARInfo>

	</UiMod>
</ModuleFile>
