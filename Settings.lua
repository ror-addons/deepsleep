
local Settings={}
DeepSleep.Settings=Settings
Settings.Descriptions={}

local SettingsParent="DeepSleep_Settings_ScrollWindow_ScrollChild"
local SettingsWindow=SettingsParent.."_"

local pairs=pairs

function Settings.Create()
	CreateWindow("DeepSleep_Settings",false)
	LabelSetText(SettingsParent.."Title",L"Prescribe sleeping pills for:")
	Settings.CreateComboBoxes()
	WindowSetShowing("DeepSleep_Settings",false)
end

function Settings.Open()
	Settings.PopulateAll()
	if(not WindowGetShowing("DeepSleep_Settings"))
	then
		WindowSetShowing("DeepSleep_Settings",true)
	end
end

function Settings.Close()
	if(WindowGetShowing("DeepSleep_Settings"))
	then
		WindowSetShowing("DeepSleep_Settings",false)
	end
end

local function compare(a,b)return a.num<b.num end
function Settings.CreateComboBoxes()
	local sorted={}
	for i,k in pairs(DeepSleep.Modules)
	do
		sorted[k.num]=k
	end
	table.sort(sorted,compare)
	local lastwindow=SettingsWindow.."Start"
	for i,k in ipairs(sorted)
	do
		local name=SettingsWindow..k.Name
		CreateWindowFromTemplate(name,"DeepSleep_Templates_CheckBox",SettingsParent)
		WindowAddAnchor(name,"bottomleft",lastwindow,"topleft",0,0)
		LabelSetText(name.."Text", StringToWString(k.Name))
		ComboBoxAddMenuItem(name.."ComboBox",L"grant")
		ComboBoxAddMenuItem(name.."ComboBox",L"deny")
		lastwindow=name
	end
end

function Settings.ComboBoxChanged()
	local box=SystemData.ActiveWindow.name
	if(ComboBoxGetDisabledFlag(box))then return end
	local idx=ComboBoxGetSelectedMenuItem(box)
	local _,_,name=string.find(box,"[_,%w]+_(%w+)ComboBox")
	Settings.ToggleModule(name,idx)
end

function Settings.PopulateAll()
	for i,k in pairs(DeepSleep.Modules)
	do
		Settings.PopulateModule(i)
	end
end

function Settings.PopulateModule(modulename)
	local modulewindow=SettingsWindow..modulename
	local box=modulewindow.."ComboBox"
	ComboBoxSetSelectedMenuItem(box,DeepSleep.data.activemodules[modulename]==true and 1 or 2)
	local ActiveLabel=modulewindow.."Active"
	local module=DeepSleep.Modules[modulename]
	local Active=module.Active
	local Check=module:Check()
	LabelSetText(ActiveLabel,Active and L"zzzZZZzzZZzz" or Check and L"demanding" or L"denied")
	LabelSetTextColor(ActiveLabel,unpack(Active and {0,150,0} or Check and {200,100,0} or {200,0,0}))
end

function Settings.ToggleModule(modulename,idx)
	DeepSleep.data.activemodules[modulename]=(idx==1)
	DeepSleep.ChangeModuleState(modulename)
	Settings.PopulateModule(modulename)
end

function Settings.OnMouseOver()
	local mouseWindow = SystemData.ActiveWindow.name
	local _,_,moduleName = string.find(mouseWindow,"[_,%w]+_(%w+)Text")
	Tooltips.CreateTextOnlyTooltip(mouseWindow, DeepSleep.Modules[moduleName].Description )
	Tooltips.AnchorTooltipManual( "topright", "CursorWindow", "topleft", 10, 10 )
end