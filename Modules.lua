
local Modules={}
DeepSleep.Modules=Modules
local Module
local count=0

local RegisterEventHandler=RegisterEventHandler
local UnregisterEventHandler=UnregisterEventHandler
local WindowUnregisterEventHandler=WindowUnregisterEventHandler

local function NewModule(name)
	count=count+1
	Module={["Name"]=name,["Default"]=true,num=count}
	Modules[name]=Module
end

local function DoNothing() end

local function DeactivateFunctions(Module)
	local dump={}
	Module.FunctionDump=dump
	for i,k in pairs(Module.Functions)
	do
		dump[i]=_G[k][i]
		_G[k][i]=DoNothing
	end
end
local function ReactivateFunctions(Module)
	local dump=Module.FunctionDump
	for i,k in pairs(Module.Functions)
	do
		_G[k][i]=dump[i]
	end
end

local function DisableEvents(Module)
	for i,k in pairs(Module.Events)
	do
		UnregisterEventHandler(i,k)
	end
end
local function ReactivateEvents(Module)
	for i,k in pairs(Module.Events)
	do
		RegisterEventHandler(i,k)
	end
end


NewModule("PlayerWindow")
Module.Description=L"This Module disables Player window related update functions when the Player window is hidden."
Module.Functions={["Update"]="PlayerWindow"}
Module.Events={}

function Module:Start()
	DeactivateFunctions(self)
	self.Active=true
end

function Module:Stop()
	ReactivateFunctions(self)
	self.Active=false
end

function Module:Check()
	local lewindow = LayoutEditor.windowsList["PlayerWindow"]
	if(lewindow and lewindow.isUserHidden)
	then
		return true
	else
		return false
	end
end


NewModule("GroupWindow")
Module.Description=L"This Module disables all group window related displays, inclusively their BuffTracker when the main group window is hidden."
Module.Functions={["Update"]="GroupMemberUnitFrame"}
Module.Events={
	[SystemData.Events.GROUP_UPDATED] = "GroupWindow.OnGroupUpdated",
	[SystemData.Events.BATTLEGROUP_UPDATED] = "GroupWindow.OnGroupUpdated",
	[SystemData.Events.GROUP_STATUS_UPDATED] = "GroupWindow.OnStatusUpdated",
	[SystemData.Events.GROUP_EFFECTS_UPDATED] = "GroupWindow.OnEffectsUpdated",
	[SystemData.Events.GROUP_PLAYER_ADDED] = "GroupWindow.OnGroupPlayerAdded",
	[SystemData.Events.SCENARIO_BEGIN] = "GroupWindow.OnScenarioBegin",
	[SystemData.Events.CITY_SCENARIO_BEGIN] = "GroupWindow.OnScenarioBegin",
	[SystemData.Events.SCENARIO_END] = "GroupWindow.OnScenarioEnd",
	[SystemData.Events.CITY_SCENARIO_END] = "GroupWindow.OnScenarioEnd",
	[SystemData.Events.SCENARIO_GROUP_JOIN] = "GroupWindow.OnScenarioGroupJoin",
	[SystemData.Events.SCENARIO_GROUP_LEAVE] = "GroupWindow.OnScenarioGroupLeave"
}

function Module:Start()
	DisableEvents(self)
	DeactivateFunctions(self)
	if(not self.ContextMenuUpdateHook)
	then
		self.ContextMenuUpdateHook=PlayerMenuWindow.AddGroupMenuItems
		PlayerMenuWindow.AddGroupMenuItems=function(...)
			GroupWindow.groupData = PartyUtils.GetPartyData()
			self.ContextMenuUpdateHook(...)
		end
	end
	self.Active=true
end

function Module:Stop()
	ReactivateFunctions(self)
	ReactivateEvents(self)
	if(PhantomLib)then PhantomLib.EnforceEventStates() end
	GroupWindow.OnGroupUpdated()
	self.Active=false
end

function Module:Check()
	local lewindow = LayoutEditor.windowsList[GroupWindow.CONTAINER_WINDOW]
	if(lewindow and lewindow.isUserHidden)
	then
		return true
	else
		return false
	end
end


NewModule("WarbandWindow")
Module.Description=L"This Module disables redundant functions when all BattlegroupHUD windows are hidden in the Layout Editor"
Module.Functions={["Update"]="BattlegroupHUD"}
Module.Events={
	[SystemData.Events.SCENARIO_BEGIN] = "BattlegroupHUD.OnScenarioBegin",
	[SystemData.Events.CITY_SCENARIO_BEGIN] = "BattlegroupHUD.OnScenarioBegin",
	[SystemData.Events.SCENARIO_END] = "BattlegroupHUD.OnScenarioEnd",
	[SystemData.Events.CITY_SCENARIO_END] = "BattlegroupHUD.OnScenarioEnd",
	[SystemData.Events.BATTLEGROUP_UPDATED] = "BattlegroupHUD.Update",
	[SystemData.Events.BATTLEGROUP_MEMBER_UPDATED] = "BattlegroupHUD.SingleMemberUpdate"
}

function Module:Start()
	DisableEvents(self)
	DeactivateFunctions(self)
	self.Active=true
end

function Module:Stop()
	ReactivateFunctions(self)
	ReactivateEvents(self)
	self.Active=false
end

function Module:Check()
	for groupIndex = 1,4 do
		local lewindow = LayoutEditor.windowsList["BattlegroupHUDGroup"..groupIndex.."LayoutWindow"]
		if(not(lewindow and lewindow.isUserHidden)
			and ButtonGetPressedFlag("EA_Window_OpenPartyManageWarband"..groupIndex.."Show"))
		then return false end		-- if ONE is visible, may not Start()
	end
	return true
end


NewModule("StandardMinimap")
Module.Default=false
Module.Description=L"This Module disables the standard minimap when it is hidden in the Layout Editor. This module is off by default because a few addons depend on an active minimap even when hidden."
Module.Functions={}
Module.Events={}

function Module:Start()
	EA_Window_OverheadMap.Shutdown()
	self.Active=true
end

function Module:Stop()
	LayoutEditor.UnregisterWindow("EA_Window_OverheadMap")

	WindowUnregisterEventHandler("EA_Window_OverheadMap", SystemData.Events.CITY_RATING_UPDATED)
	WindowUnregisterEventHandler("EA_Window_OverheadMap", SystemData.Events.TOGGLE_WORLD_MAP_WINDOW)
	WindowUnregisterEventHandler("EA_Window_OverheadMap", SystemData.Events.LOADING_END)
	WindowUnregisterEventHandler("EA_Window_OverheadMap", SystemData.Events.USER_SETTINGS_CHANGED)
	WindowUnregisterEventHandler("EA_Window_OverheadMap", SystemData.Events.SCENARIO_BEGIN)
	WindowUnregisterEventHandler("EA_Window_OverheadMap", SystemData.Events.SCENARIO_END)
	WindowUnregisterEventHandler("EA_Window_OverheadMap", SystemData.Events.PLAYER_RVR_FLAG_UPDATED)
	WindowUnregisterEventHandler("EA_Window_OverheadMap", SystemData.Events.INTERACT_UPDATED_SCENARIO_QUEUE_LIST)
	WindowUnregisterEventHandler("EA_Window_OverheadMap", SystemData.Events.SCENARIO_ACTIVE_QUEUE_UPDATED)
	WindowUnregisterEventHandler("EA_Window_OverheadMap", SystemData.Events.PLAYER_AREA_NAME_CHANGED)
	WindowUnregisterEventHandler("EA_Window_OverheadMap", SystemData.Events.PLAYER_ZONE_CHANGED)
	WindowUnregisterEventHandler("EA_Window_OverheadMap", SystemData.Events.MAILBOX_UNREAD_COUNT_CHANGED)
	WindowUnregisterEventHandler("EA_Window_OverheadMap", SystemData.Events.RALLY_CALL_INVITE)
	WindowUnregisterEventHandler("EA_Window_OverheadMap", SystemData.Events.PLAYER_CAREER_RANK_UPDATED)
	WindowUnregisterEventHandler("EA_Window_OverheadMap", SystemData.Events.TOGGLE_CURRENT_EVENTS_WINDOW)    
	WindowUnregisterEventHandler("EA_Window_OverheadMap", SystemData.Events.CURRENT_EVENTS_LIST_UPDATED)

	EA_Window_OverheadMap.Initialize()
	self.Active=false
end

function Module:Check()
	local lewindow = LayoutEditor.windowsList["EA_Window_OverheadMap"]
	if(lewindow and lewindow.isUserHidden)
	then
		return true
	else
		return false
	end
end
