
DeepSleep = {}
local DeepSleep=DeepSleep

local pairs = pairs
local pcall = pcall

function DeepSleep.Initialize()
	if(DeepSleep.data==nil)
	then
		DeepSleep.data=	{
								activemodules	={}
							}
	end

	DeepSleep.RefreshModules()
	if(LibSlash)
	then
		LibSlash.RegisterSlashCmd("ds",DeepSleep.HandleSlash)
		LibSlash.RegisterSlashCmd("DeepSleep",DeepSleep.HandleSlash)
	end

	LayoutEditor.RegisterEditCallback( DeepSleep.LayoutEditorDone )

	local callback=EA_Window_OpenPartyManage.ToggleWarbandVisibility
	EA_Window_OpenPartyManage.ToggleWarbandVisibility=function(...)
		callback(...)
		DeepSleep.RefreshModules()
	end
end

function DeepSleep.Shutdown()
	DeepSleep.Settings.Close()
	if(LibSlash)
	then
		LibSlash.UnregisterSlashCmd("ds")
		LibSlash.UnregisterSlashCmd("DeepSleep")
	end
end

function DeepSleep.SafeCall(...)
	local result,msg=pcall(...)
	if(not result)
	then
		LogLuaMessage("Lua",SystemData.UiLogFilters.ERROR,towstring(msg))
	end
end

function DeepSleep.HandleSlash(input)
	local opt, val = input:match("([a-z0-9]+)[ ]?(.*)")

	if not(opt)then
		if(DeepSleep.Settings) then
			DeepSleep.Settings.Open()
		else
			TextLogAddEntry("Chat", 0, L"An error occurred while creating the windowstingswindow")
		end
	end
end

function DeepSleep.RefreshModules()
	for i,k in pairs(DeepSleep.Modules)
	do
		DeepSleep.ChangeModuleState(i,k)
	end
	if(DoesWindowExist("DeepSleep_Settings") and WindowGetShowing("DeepSleep_Settings"))
	then
		DeepSleep.Settings.PopulateAll()
	end
end

function DeepSleep.ChangeModuleState(modulename,module)
	local Automatic=DeepSleep.data.activemodules[modulename]
	module=module or DeepSleep.Modules[modulename]
	if(Automatic==nil)
	then
		Automatic=module.Default
		DeepSleep.data.activemodules[modulename]=Automatic
	end
	local Active=(module.Active or false)
	if(Active and not Automatic)
	then
		DeepSleep.SafeCall(module.Stop,module)
	else
		if(Automatic and (module:Check() ~= Active))
		then
			if(module.Active or not Automatic)
			then
				DeepSleep.SafeCall(module.Stop,module)
			else
				DeepSleep.SafeCall(module.Start,module)
			end
		end
	end
end

function DeepSleep.LayoutEditorDone(event)
	if (LayoutEditor.EDITING_END ~= event) then return end
	DeepSleep.RefreshModules()
end
